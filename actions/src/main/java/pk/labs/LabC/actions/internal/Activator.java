package pk.labs.LabC.actions.internal;

import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

public class Activator implements BundleActivator {

    private static BundleContext Context;

    @Override
    public void start(BundleContext bc) {

        Context = bc;

        AnimalAction zwierz1 = new AnimalAction() {
            public String toString() {
                return "Jedz";
            }

 

            public boolean execute(Animal animal) {
                animal.setStatus("Jedz");
                return true;
            }
        };

        AnimalAction zwierz2 = new AnimalAction() {
            public String toString() {
                return "Pij";
            }

            public boolean execute(Animal animal) {
                animal.setStatus("Pij");
                return true;
            }
        };

        AnimalAction zwierz3 = new AnimalAction() {
            public String toString() {
                return "Skacz";
            }

            public boolean execute(Animal animal) {
                animal.setStatus("Skacz");
                return true;
            }
        };

        Hashtable propa1 = new Hashtable();
        propa1.put("species", "Krolik");
        Hashtable propa2 = new Hashtable();
        propa2.put("species", new String[]{"Pajak"});
        Hashtable propa3 = new Hashtable();
        propa3.put("species", "Lama");
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), zwierz1, propa1);
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), zwierz2, propa2);
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), zwierz3, propa3);

    }

    @Override
    public void stop(BundleContext bc) {
        Context = null;
    }
}
