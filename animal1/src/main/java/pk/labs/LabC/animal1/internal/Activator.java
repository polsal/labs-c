package pk.labs.LabC.animal1.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;


public class Activator implements BundleActivator{
    
    private BundleContext Context;

    @Override
    public void start(BundleContext bc) throws Exception {
        Context = bc;
        Context.registerService(Animal.class.getName(), new Krolik(), null);
        pk.labs.LabC.logger.Logger.get().log(this, "Krolik dokica");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        Context = null;
        pk.labs.LabC.logger.Logger.get().log(this, "Krolik odkicuje");
    }
    
}
