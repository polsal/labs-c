/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal3.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;


public class Activator implements BundleActivator{
    
    private BundleContext Context;

    @Override
    public void start(BundleContext bc) throws Exception {
        Context = bc;
        Context.registerService(Animal.class.getName(), new Lama(), null);
        pk.labs.LabC.logger.Logger.get().log(this, "Lama nadciąga");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        Context = null;
        pk.labs.LabC.logger.Logger.get().log(this, "Lama odchodzi");
    }
    
}
